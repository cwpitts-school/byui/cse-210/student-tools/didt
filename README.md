# didt

Did I Document That? is a tool to help students ensure that they have
documented every module, class, and function in their code. It walks
the abstract syntax tree of a directory, checking for appropriate
docstrings at the relevant tree nodes.

## Usage
Basic usage is as follow:

```
# To parse the current directory
didt .

# To parse "foo"
didt foo/
```

Example output from running `didt` on itself:

```
-> % didt .
setup.py
__main__.py
__init__.py
-> % 
```
