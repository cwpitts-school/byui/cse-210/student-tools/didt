""" Documentation checker

A command-line tool for checking source code documentation requirements.

Authors:
  - Christopher Pitts
"""
import logging
from argparse import ArgumentParser, Namespace
from functools import partial
from pathlib import Path
from pprint import pformat, pprint
from typing import Any, Dict, List

import astroid
from rich.console import Console

__version__ = "0.1.0"

log: logging.Logger = logging.getLogger("didt")
logging.basicConfig()


def existing_path(path: Any) -> Path:
    """Check if path exists

    Args:
      path (Any): Any object that can be converted to a Path

    Returns:
      path (Path): Resolved Path object

    Raises:
      ValueError: When the path does not exist
    """
    if not isinstance(path, Path):
        path = Path(path)

    if not path.exists():
        raise ValueError(f"Path {path} not found")

    return path


def parse_args(argv: List[str] = None) -> Namespace:
    """Parse and validate CLI args

    Args:
      argv (List[str], optional): Argv for parsing, defaults to None

    Returns:
      Namespace: Parsed and validate arguments

    Notes:
      The default value of argv will cause the parser to read from stdin.
    """
    argp: ArgumentParser = ArgumentParser(prog="didt")
    argp.add_argument("source_dir", help="path to source directory", type=existing_path)
    argp.add_argument(
        "-d", "--debug", help="run in debug mode", action="store_true", default=False
    )

    return argp.parse_args(argv)


def check_module(filepath: Path) -> Dict[str, Any]:
    """ Walk the AST for a module and track undocumented items

    Args:
      filepath (Path): The path to a module file to check

    Returns:
      ret (Dict[str, List[str]]): Items without required documentation
    """
    filepath_s: str = str(filepath)
    ret: Dict[str, List[str]] = {
        "modules": [],
        "classes": [],
        "functions": [],
    }
    with open(filepath, "r") as in_file:
        module: astroid.Node = astroid.parse(in_file.read())

        if not module.doc:
            ret["modules"].append(filepath_s)

        for node in module.body:
            # Function definitions always have to have the docstring
            if isinstance(node, astroid.FunctionDef):
                if not node.doc:
                    ret["functions"].append(node)
            elif isinstance(node, astroid.ClassDef):
                # Just keep track of the class docstring, we will use this in a check later
                has_class_level_doc: bool = node.doc is not None

                for class_subnode in node.body:
                    # Each class method should have a docstring
                    if isinstance(class_subnode, astroid.FunctionDef):
                        if class_subnode.name == "__init__":
                            # The init method docstring counts against the requirement
                            # for the class-level docstring
                            if not (class_subnode.doc or has_class_level_doc):
                                ret["classes"].append(node)
                                ret["functions"].append(
                                    class_subnode
                                )
                        else:
                            if not class_subnode.doc:
                                ret["functions"].append(
                                    class_subnode
                                )

    return ret

def main(args: Namespace = None):
    """Main program logic

    Args:
      args (Namespace): Program arguments
    """
    if not args:
        args = parse_args()

    if args.debug:
        log.setLevel(logging.DEBUG)

    log.debug("Running with arguments:\n%s", pformat(vars(args)))

    results = {}
    for filepath in args.source_dir.rglob("*.py"):
        results[str(filepath)] = check_module(filepath)

    c = Console()
    for filename, items in results.items():
        c.print(f"[green]{filename}[/green]")
        if items["modules"]:
            c.print(f"[red]- No module docstring[/red]")
        if items["classes"]:
            c.print(f"[red]- {len(items['classes'])} undocumented class(es):[/red]")
            for item in items["classes"]:
                c.print(f"[red]  - {item.name}, line {item.lineno}[/red]")
        if items["functions"]:
            c.print(f"[red]- {len(items['functions'])} undocumented function(s):[/red]")
            for item in items["functions"]:
                c.print(f"[red]  - {item.name}, line {item.lineno}[/red]")


if __name__ == "__main__":
    _args: Namespace = parse_args()
    main(_args)
